var gulp = require('gulp');

var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cleanCSS = require('gulp-clean-css');

var browserSync = require('browser-sync').create();

var wait = require('gulp-wait');

gulp.task('browserSync', function() {
	browserSync.init({
		server: {
		  baseDir: 'prod'
		}
	})
})

gulp.task('sass', function(){
	return gulp.src('src/styles/main.scss')
	.pipe(wait(200))
	.pipe(sass()).on('error', sass.logError)
	.pipe(autoprefixer({
		browsers: ['last 2 versions'],
		cascade: false
	}))
	.pipe(cleanCSS({compatibility: 'ie10'}))
	.pipe(gulp.dest('prod/styles'))
	.pipe(browserSync.reload({
      stream: true
    }))
});

gulp.task('watch', ['browserSync', 'sass'], function (){
	gulp.watch('src/styles/**/*.scss', ['sass']); 
	gulp.watch('prod/*.html', browserSync.reload); 
});